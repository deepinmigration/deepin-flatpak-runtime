all: platform sdk

clean:
	rm -rf repo exportrepo 
	sudo rm -rf buildroot buildroot-prepare .commit-*
	sudo rm -rf debootstrap

VERSION:=1
ARCH:=x86_64
DIST:=unstable
MIRROR:=http://pools.corp.deepin.com/deepin
APT_OPTIONS:= 

repo/config:
	ostree init --repo=repo --mode=bare-user

exportrepo/config:
	ostree init --repo=exportrepo --mode=archive-z2

debootstrap:
	sudo debootstrap --merged-usr --no-check-gpg $(DIST) debootstrap  $(MIRROR)

repo/refs/heads/base/com.deepin.Runtime/$(ARCH)/$(VERSION): debootstrap repo/config config.sh runtime.packages
	sudo rm -rf buildroot buildroot-prepare
	sudo mkdir buildroot buildroot-prepare
	sudo cp -r debootstrap/* buildroot
	sudo install runtime.packages buildroot/root/packages.list
	sudo chroot buildroot xargs --arg-file=/root/packages.list apt-get ${APT_OPTIONS} install
	sudo chroot buildroot apt-get clean
	#run config.sh
	sudo install -Dm755 config.sh buildroot/root/config.sh
	sudo chroot buildroot /root/config.sh
	sudo rm -f buildroot/root/config.sh
	sudo rm -f buildroot/root/packages.list
	sudo mv buildroot/usr buildroot-prepare/usr
	sudo mv buildroot/etc buildroot-prepare/etc
	sudo ostree --repo=repo commit -s 'initial build' -b base/com.deepin.Runtime/$(ARCH)/$(VERSION) --tree=dir=$$PWD/buildroot-prepare
	sudo chown -R `whoami` repo

repo/refs/heads/base/com.deepin.Sdk/$(ARCH)/$(VERSION): debootstrap  repo/config config.sh sdk.packages
	sudo rm -rf buildroot buildroot-prepare
	sudo mkdir buildroot buildroot-prepare
	sudo cp -r debootstrap/* buildroot
	sudo install sdk.packages buildroot/root/packages.list
	sudo chroot buildroot xargs --arg-file=/root/packages.list apt-get ${APT_OPTIONS} install
	sudo chroot buildroot apt-get clean
	sudo install -Dm755 config.sh buildroot/root/config.sh
	sudo chroot buildroot /root/config.sh
	sudo rm -f buildroot/root/config.sh
	sudo rm -f buildroot/root/packages.list
	sudo cp -r buildroot/usr buildroot-prepare/
	sudo cp -r buildroot/etc buildroot-prepare/
	sudo mkdir -p buildroot-prepare/var
	sudo cp -r buildroot/var/lib buildroot-prepare/var
	sudo ostree --repo=repo commit -s 'initial build' -b base/com.deepin.Sdk/$(ARCH)/$(VERSION) --tree=dir=$$PWD/buildroot-prepare
	sudo chown -R `whoami` repo

repo/refs/heads/runtime/com.deepin.Runtime/$(ARCH)/$(VERSION): repo/refs/heads/base/com.deepin.Runtime/$(ARCH)/$(VERSION) metadata.runtime
	./commit-subtree.sh base/com.deepin.Runtime/$(ARCH)/$(VERSION) runtime/com.deepin.Runtime/$(ARCH)/$(VERSION) metadata.runtime /usr files

repo/refs/heads/runtime/com.deepin.Runtime.Var/$(ARCH)/$(VERSION): repo/refs/heads/base/com.deepin.Runtime/$(ARCH)/$(VERSION) metadata.runtime
	./commit-subtree.sh base/com.deepin.Runtime/$(ARCH)/$(VERSION) runtime/com.deepin.Runtime.Var/$(ARCH)/$(VERSION) metadata.runtime /var files

repo/refs/heads/runtime/com.deepin.Sdk/$(ARCH)/$(VERSION): repo/refs/heads/base/com.deepin.Sdk/$(ARCH)/$(VERSION) metadata.sdk
	./commit-subtree.sh base/com.deepin.Sdk/$(ARCH)/$(VERSION) runtime/com.deepin.Sdk/$(ARCH)/$(VERSION) metadata.sdk /usr files

repo/refs/heads/runtime/com.deepin.Sdk.Var/$(ARCH)/$(VERSION): repo/refs/heads/base/com.deepin.Sdk/$(ARCH)/$(VERSION) metadata.sdk
	./commit-subtree.sh base/com.deepin.Sdk/$(ARCH)/$(VERSION) runtime/com.deepin.Sdk.Var/$(ARCH)/$(VERSION) metadata.sdk /var files

exportrepo/refs/heads/runtime/com.deepin.Runtime/$(ARCH)/$(VERSION): repo/refs/heads/runtime/com.deepin.Runtime/$(ARCH)/$(VERSION) exportrepo/config
	ostree pull-local --repo=exportrepo repo runtime/com.deepin.Runtime/$(ARCH)/$(VERSION)
	flatpak build-update-repo exportrepo

exportrepo/refs/heads/runtime/com.deepin.Runtime.Var/$(ARCH)/$(VERSION): repo/refs/heads/runtime/com.deepin.Runtime.Var/$(ARCH)/$(VERSION) exportrepo/config
	ostree pull-local --repo=exportrepo repo runtime/com.deepin.Runtime.Var/$(ARCH)/$(VERSION)
	flatpak build-update-repo exportrepo

exportrepo/refs/heads/runtime/com.deepin.Sdk/$(ARCH)/$(VERSION): repo/refs/heads/runtime/com.deepin.Sdk/$(ARCH)/$(VERSION) exportrepo/config
	ostree pull-local --repo=exportrepo repo runtime/com.deepin.Sdk/$(ARCH)/$(VERSION)
	flatpak build-update-repo exportrepo

exportrepo/refs/heads/runtime/com.deepin.Sdk.Var/$(ARCH)/$(VERSION): repo/refs/heads/runtime/com.deepin.Sdk.Var/$(ARCH)/$(VERSION) exportrepo/config
	ostree pull-local --repo=exportrepo repo runtime/com.deepin.Sdk.Var/$(ARCH)/$(VERSION)
	flatpak build-update-repo exportrepo

platform: exportrepo/refs/heads/runtime/com.deepin.Runtime/$(ARCH)/$(VERSION) 

sdk: exportrepo/refs/heads/runtime/com.deepin.Sdk/$(ARCH)/$(VERSION) exportrepo/refs/heads/runtime/com.deepin.Sdk.Var/$(ARCH)/$(VERSION)
