#!/bin/bash
# fix awk wrong links to /etc/alternatives
for i in $(ls /usr/bin);do
	if  [ -L /usr/bin/$i ] && ls -l /usr/bin/$i | grep /etc/alternatives ;then
		ln -sf $(readlink -f /usr/bin/$i) /usr/bin/$i
	fi
done

# Configure fontconfig to look in /app
mkdir -p /app/cache/fontconfig/
mkdir -p /app/etc/fonts/conf.d/
cat >/app/etc/fonts/conf.d/50-flatpak.conf <<EOF
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
        <cachedir>/usr/cache/fontconfig</cachedir>

        <dir>/app/share/fonts</dir>
        <cachedir>/app/cache/fontconfig</cachedir>

        <include ignore_missing="yes">/app/etc/fonts/local.conf</include>

        <dir>/run/host/fonts</dir>
</fontconfig>
EOF

# Configure ld.so to look in /app
mkdir -p /app/etc/ld.so.conf.d/
echo "/app/lib" > /app/etc/ld.so.conf.d/app.conf
/sbin/ldconfig
